package http.client;

import javaFX.helper.classes.HttpReqMethod;
import javaFX.helper.classes.MoveConditions;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by Andrei-Mihai Nagy on 07.07.2017.
 */
public class RunHttpRequestSingleton implements Runnable
{
    private String url;
    private HttpReqMethod method;
    private String responseMessage = null;
    private int responseCode = -1;

    private static final RunHttpRequestSingleton instance = new RunHttpRequestSingleton();

    public static RunHttpRequestSingleton getInstance()
    {
        return instance;
    }

    private RunHttpRequestSingleton()
    {
    }

    public String getResponseMessage()
    {
        return responseMessage;
    }

    public int getResponseCode()
    {
        return responseCode;
    }

    public void start(HttpReqMethod method, String url)
    {
        this.url = url;
        this.method = method;
        new Thread(this).start();
    }

    @Override
    public void run()
    {
        try
        {
            Request httpRequest = null;
            switch (method)
            {
                case GET:
                    httpRequest = new Request.Builder()
                            .addHeader("Authorization", "Bearer " + MoveConditions.authToken)
                            .url(url)
                            .build();
                    break;
                case PUT:
                    RequestBody body = RequestBody.create(null, new byte[0]); // empty request body
                    httpRequest = new Request.Builder()
                            .addHeader("Authorization", "Bearer " + MoveConditions.authToken)
                            .url(url)
                            .put(body)
                            .build();
                    break;
            }

            Response httpResponse = new okhttp3.OkHttpClient().newCall(httpRequest).execute();
            responseMessage = httpResponse.body().string();
            responseCode = httpResponse.code();
            System.out.println("Code: " + responseCode + "\n" + responseMessage);
        } catch (IOException | NullPointerException e)
        {
            e.printStackTrace();
        }
    }
}