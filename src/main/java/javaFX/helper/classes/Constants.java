package javaFX.helper.classes;

/**
 * Created by Andrei-Mihai Nagy on 07.07.2017.
 */
public class Constants
{
    // PIXEL sizes -----------------------------------------------------------------------------------------------------
    public static final int WINDOW_WIDTH = 720;
    public static final int WINDOW_HEIGHT = 756;
    public static final int PARKING_LOT_SIZE = WINDOW_WIDTH; // parking lot background image is square so height and width are the same (thus named  parking_lot_size)
    public static final int CAR_ICON_SIZE = 160; // car icon is square so height and width are the same (thus named car_icon_size)
    public static final int OMiLAB_LOGO_HEIGHT = 36;

    // FXML files ------------------------------------------------------------------------------------------------------
    public static final String MAIN_WINDOW_PATH = "/mainWindow.fxml";
    public static final String AUTHENTICATON_WINDOW_PATH = "/authenticationWindow.fxml";

    // Various OMiLAB URLs ---------------------------------------------------------------------------------------------
    public static final String URL_OMiLAB_HOMEPAGE = "http://austria.omilab.org/psm/home";
    public static final String URL_PROJECT_WEBPAGE = "http://austria.omilab.org/psm/content/omicar1/";
    public static final String URL_GET_TOKEN = "http://austria.omilab.org/psm/content/omicar1/omirob-car1?view=car-auth1";

    // URLs for HTTP Get requests --------------------------------------------------------------------------------------
    private static final String MBOT_SPEED = "85";
    public static final String URL_PARK_YOURSELF = "http://austria.omilab.org/omirob/car1/rest/parkYourself2/" + MBOT_SPEED;
    public static final String URL_PARK_IN_X = "http://austria.omilab.org/omirob/car1/rest/park/" + MBOT_SPEED + "/";
    public static final String URL_PARKED_IN_SPOT = "http://austria.omilab.org/omirob/car1/rest/getParkedInSpot";
    public static final String URL_TOKEN_TEST = "http://austria.omilab.org/omirob/car1/rest/tokenTest";

    // URLs for images -------------------------------------------------------------------------------------------------
    public static final String CAR_ICON_URL = "https://i.imgur.com/VvDbWaY.png";
    public static final String HELP_ICON_URL = "https://i.imgur.com/INPaDgX.png";
    public static final String PARK_ICON_URL = "https://i.imgur.com/9lnbLQ9.png";
    public static final String AUTH_ICON_URL = "https://i.imgur.com/O8QNeI8.png";
    public static final String SUCCESS_ICON_URL = "https://i.imgur.com/JoT0BjV.png";
    public static final String FAILURE_ICON_URL = "http://i.imgur.com/3gUNniZ.png";
    public static final String OMiLAB_LOGO_URL = "https://i.imgur.com/8soyO3L.png";

    // The text in the Help window -------------------------------------------------------------------------------------
    public static final String HELP_TEXT = "1. If the car icon has not been moved from its starting point click the letter \'P\' on your keyboard to start the self-parking process.\n\n" +
            "OR\n\n" +
            "2. Use UP, DOWN, LEFT, RIGHT arrows as follows to move the car icon (there is no need to keep the keys pressed):\n" +
            "       UP makes the car icon move forwards until another key is pressed\n" +
            "       DOWN makes the car icon move backwards until another key is pressed\n" +
            "       LEFT turns the car icon left by 90 degrees if it is not currently moving\n" +
            "       RIGHT turns the car icon right by 90 degrees if it is not currently moving\n\n" +
            "Note: Every one of this keys stops the car icon if it is currently moving (e.g. UP key moves the car icon forwards if it is not currently moving, otherwise it stops the current movement. The same is true for all other keys).\n" +
            "Once you have maneuvered the car icon into a parking spot a new button \'Park here\' appears in the top right corner of the window. Click it to start the parking process for the mBot.";

    // OTHERS ----------------------------------------------------------------------------------------------------------
    public static final int HTTP_STATUS_SUCCESS = 200;
    public static final int MILLIS_FOR_90DEG_TURN = 1000; // used in the RotateTransition to ensure a more smooth 90 degree turn
}