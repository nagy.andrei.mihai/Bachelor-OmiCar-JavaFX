package javaFX.controllers;

import http.client.RunHttpRequestSingleton;
import javaFX.helper.classes.Constants;
import javaFX.helper.classes.HttpReqMethod;
import javaFX.helper.classes.MoveConditions;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Andrei-Mihai Nagy on 17.07.2017.
 */
public class AuthenticationWindowController
{
    public TextField tokenTextField;
    public Text feedbackTest;
    public Button okButton;
    public Button testTokenButton;
    public Button getTokenButton;

    /**
     * Method executed when the 'Test token' button is clicked.
     * A new HTTP Get request is sent using the entered token.
     * If the request is successful is enabled.
     * If the request is not successful it means that the token is wrong and should be entered anew.
     */
    public void testTokenButtonClicked()
    {
        feedbackTest.setText("WAIT FOR RESPONSE");
        MoveConditions.authToken = tokenTextField.getText();
        RunHttpRequestSingleton runHttpRequest = RunHttpRequestSingleton.getInstance();
        runHttpRequest.start(HttpReqMethod.GET, Constants.URL_TOKEN_TEST);
        trySleep(1000);
        if (runHttpRequest.getResponseCode() != Constants.HTTP_STATUS_SUCCESS)
            feedbackTest.setText("TOKEN IS WRONG");
        else
        {
            feedbackTest.setFill(Color.GREEN);
            feedbackTest.setText("TOKEN IS OK");
            okButton.setDisable(false);
            testTokenButton.setDisable(true);
            getTokenButton.setDisable(true);
        }
    }

    public void okButtonClicked()
    {
        ((Stage) okButton.getScene().getWindow()).close();
    }

    public void getTokenButtonClicked()
    {
        openInBrowser(Constants.URL_GET_TOKEN);
    }

    public void omilabImageViewClicked()
    {
        openInBrowser(Constants.URL_OMiLAB_HOMEPAGE);
    }

    private void trySleep(int ms)
    {
        try
        {
            Thread.sleep(ms);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Method opens in default browser the URI passed as string parameter.
     *
     * @param uri URI to be opened in the default browser.
     */
    private void openInBrowser(String uri)
    {
        try
        {
            Desktop.getDesktop().browse(new URI(uri));
        } catch (IOException | URISyntaxException e1)
        {
            e1.printStackTrace();
        }
    }
}
