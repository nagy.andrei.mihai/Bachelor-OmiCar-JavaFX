package main;

import http.client.RunHttpRequestSingleton;
import javaFX.helper.classes.Constants;
import javaFX.helper.classes.HttpReqMethod;
import javaFX.helper.classes.MoveConditions;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Created by Andrei-Mihai Nagy on 07.07.2017.
 */
public class ParkingAppMain extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception
    {
        Parent root = new FXMLLoader().load(this.getClass().getResource(Constants.MAIN_WINDOW_PATH).openStream());
        Scene scene = new Scene(root, Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
        //Scene scene = new Scene(root);

        scene.setOnKeyReleased(keyEvent ->
        {
            switch (keyEvent.getCode())
            {
                case UP: // move forwards (or stop if currently moving)
                    if (!MoveConditions.parkYourself)
                        if (!MoveConditions.moving)
                        {
                            MoveConditions.setMoveForwards();
                            MoveConditions.moving = true;
                            MoveConditions.carHasLeftInitialState = true;
                        } else stopCar();
                    break;
                case DOWN: // move backwards (or stop if currently moving)
                    if (!MoveConditions.parkYourself)
                        if (!MoveConditions.moving)
                        {
                            MoveConditions.setMoveBackwards();
                            MoveConditions.moving = true;
                            MoveConditions.carHasLeftInitialState = true;
                        } else stopCar();
                    break;
                case LEFT: // turn left by 90 degrees (or stop if currently moving)
                    if (!MoveConditions.parkYourself)
                        if (!MoveConditions.moving)
                        {
                            MoveConditions.setTurnLeft90Degrees();
                            MoveConditions.rotated = false;
                            MoveConditions.carHasLeftInitialState = true;
                        } else stopCar();
                    break;
                case RIGHT: // turn right by 90 degrees (or stop if currently moving)
                    if (!MoveConditions.parkYourself)
                        if (!MoveConditions.moving)
                        {
                            MoveConditions.setTurnRight90Degrees();
                            MoveConditions.rotated = false;
                            MoveConditions.carHasLeftInitialState = true;
                        } else stopCar();
                    break;
                case P: // start self parking process (or stop if currently moving). Note: process can only be started if the car icon is in its initial state
                    if (!MoveConditions.moving && !MoveConditions.carHasLeftInitialState)
                    {
                        MoveConditions.parkYourself = true;
                        MoveConditions.carHasLeftInitialState = true;
                        RunHttpRequestSingleton.getInstance().start(HttpReqMethod.PUT, Constants.URL_PARK_YOURSELF);
                    } else stopCar();
                    break;
                default: // all keys stop the car icon if it is currently moving
                    if (!MoveConditions.parkYourself)
                        if (MoveConditions.moving) stopCar();
                    break;
            }
        });

        stage.setTitle("Parking App");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.getIcons().add(new Image(Constants.PARK_ICON_URL)); // top left window icon

        stage.show();
    }

    private void stopCar() // assuming car is currently moving
    {
        MoveConditions.stopCar();
        MoveConditions.moving = false;
    }
}
